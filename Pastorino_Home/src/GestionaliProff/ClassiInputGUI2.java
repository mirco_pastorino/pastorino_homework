package GestionaliProff;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class ClassiInputGUI2 {
	
	private JFrame frame;
	//private Vector<Classe> classi;
	private DatiP dati;
	private Integer[] anni = {1, 2, 3, 4, 5};
	private Character[] sezioni = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'Z'}; 
	private String[] indirizzi = {"INFORMATICA", "ELETTRONICA", "ELETTROTECNICA", "MECCANICA"};
	public ClassiInputGUI2() {
		super();
		
		//classi = new Vector<>();
		dati = readDati();
		if(dati == null) dati = new DatiP();
		
		frame = new JFrame("ClassiInputGUI2");
		
		JPanel southPanel = new JPanel();
		JButton addButton = new JButton("ADD");
		JButton cancelButton = new JButton("CANCEL");
		JButton saveButton = new JButton("SAVE AND EXIT");
		
		southPanel.add(addButton);
		southPanel.add(cancelButton);
		southPanel.add(saveButton);
		
		frame.getContentPane().add(southPanel, BorderLayout.SOUTH);
		
		JPanel centerPanel = new JPanel();
		Box centerBox = Box.createVerticalBox();
		centerBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
		Box annoBox = Box.createHorizontalBox();
		Box sezioneBox = Box.createHorizontalBox();
		Box indirizzoBox = Box.createHorizontalBox();
	
		JLabel annoLabel = new JLabel("ANNO");
		JComboBox annoComboBox = new JComboBox(anni);
		annoComboBox.setSelectedIndex(-1);
		annoBox.add(annoLabel);
		annoBox.add(Box.createRigidArea(new Dimension(10, 0))); 
		annoBox.add(annoComboBox);
		
		JLabel sezioneLabel = new JLabel("SEZIONE");
		JComboBox sezioneComboBox = new JComboBox(sezioni);
		sezioneComboBox.setSelectedIndex(-1);
		sezioneBox.add(sezioneLabel);
		sezioneBox.add(Box.createRigidArea(new Dimension(10, 0))); 
		sezioneBox.add(sezioneComboBox);
		
		JLabel indirizzoLabel = new JLabel("INDIRIZZO");
		JComboBox indirizzoComboBox = new JComboBox(indirizzi);
		indirizzoComboBox.setSelectedIndex(-1);
		indirizzoBox.add(indirizzoLabel);
		indirizzoBox.add(Box.createRigidArea(new Dimension(10, 0))); 
		indirizzoBox.add(indirizzoComboBox);
		
		centerBox.add(annoBox);
		centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
		centerBox.add(sezioneBox);
		centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
		centerBox.add(indirizzoBox);
		
		centerPanel.add(centerBox);
		
		frame.getContentPane().add(centerPanel);
		
		frame.setBounds(400, 200, 300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Integer anno = (Integer) annoComboBox.getSelectedItem();
				if(anno == null) return;
				
				Character sezione = (Character) sezioneComboBox.getSelectedItem();
				if(sezione == null) return;
				
				String indirizzo = (String) indirizzoComboBox.getSelectedItem();
				if(indirizzo == null) return;
				
				//classi.add(new Classe(anno, sezione, indirizzo));
				dati.add(new Classe(anno, sezione, indirizzo));
				
				annoComboBox.setSelectedIndex(-1);
				sezioneComboBox.setSelectedIndex(-1);
				indirizzoComboBox.setSelectedIndex(-1);
				
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				annoComboBox.setSelectedIndex(-1);
				sezioneComboBox.setSelectedIndex(-1);
				indirizzoComboBox.setSelectedIndex(-1);
				
			}
		});
		
		saveButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				try {
					FileOutputStream out;
					out = new FileOutputStream("G:/Roba/Informatica/Info_2017/src/GestionaliProff/dati.dat");
					ObjectOutputStream outputStream = new ObjectOutputStream(out);
					//outputStream.writeObject(classi);
					outputStream.writeObject(dati);
					outputStream.flush();
					outputStream.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				System.exit(1);
				
				//frame.dispose();
				
			}
		});
	}
	
	public DatiP readDati() {
		
		ObjectInputStream objectInputStream = null;
		FileInputStream fileInputStream = null;
		DatiP dati = null;
		
		try {
			fileInputStream = new FileInputStream("G:/Roba/Informatica/Info_2017/src/GestionaliProff/dati.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		try {
			objectInputStream = new ObjectInputStream(fileInputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {

			dati =  (DatiP) objectInputStream.readObject();
			System.out.println(dati);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			objectInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dati;
	}
	
	public static void main(String[] args) {
		new ClassiInputGUI2();
		
	}

}
