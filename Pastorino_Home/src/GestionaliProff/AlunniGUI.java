package GestionaliProff;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class AlunniGUI {
	
		
		private JFrame frame;
		//private Vector<Classe> classi;
		private DatiP dati;
		
		
		
		public AlunniGUI() {
			super();
			
			dati = readDati();
			
			frame = new JFrame("AlunniInputGUI");
			
			JPanel southPanel = new JPanel();
			JButton addButton = new JButton("ADD");
			JButton cancelButton = new JButton("CANCEL");
			JButton saveButton = new JButton("SAVE AND EXIT");
			
			southPanel.add(addButton);
			southPanel.add(cancelButton);
			southPanel.add(saveButton);
			
			frame.getContentPane().add(southPanel, BorderLayout.SOUTH);
			
			JPanel centerPanel = new JPanel();
			Box centerBox = Box.createVerticalBox();
			centerBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
			Box nomeBox = Box.createHorizontalBox();
			Box cognomeBox = Box.createHorizontalBox();
		
			JLabel annoLabel = new JLabel("NOME");
			JTextField nomeTextField = new JTextField(6);
			nomeBox.add(annoLabel);
			nomeBox.add(Box.createRigidArea(new Dimension(10, 0))); 
			nomeBox.add(nomeTextField);
			
			JLabel sezioneLabel = new JLabel("COGNOME");
			JTextField cognomeTextField = new JTextField(6);
			cognomeBox.add(sezioneLabel);
			cognomeBox.add(Box.createRigidArea(new Dimension(10, 0))); 
			cognomeBox.add(cognomeTextField);
			
			centerBox.add(nomeBox);
			centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
			centerBox.add(cognomeBox);
			centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
			
			centerPanel.add(centerBox);
			
			frame.getContentPane().add(centerPanel);
			
			frame.setBounds(400, 200, 300, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
			
			addButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
									
					String nome = nomeTextField.getText();
										
					String cognome = cognomeTextField.getText();
				
					dati.add(new Alunno(nome, cognome));
					
					nomeTextField.setText("");
					cognomeTextField.setText("");	
				}
			});
			
			cancelButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					nomeTextField.setText("");
					cognomeTextField.setText("");
					
				}
			});
			
			saveButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					System.out.println(dati);
					
					try {
						FileOutputStream out;
						out = new FileOutputStream("G:/Roba/Informatica/Info_2017/src/GestionaliProff/dati.dat");
						ObjectOutputStream outputStream = new ObjectOutputStream(out);
			
						outputStream.writeObject(dati);
						outputStream.flush();
						outputStream.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					System.exit(0);
					
					//frame.dispose();
					
				}
			});
		}
		/*
		 *lettura di dati da file
		 */
		public DatiP readDati() {
			
			ObjectInputStream objectInputStream = null;
			FileInputStream fileInputStream = null;
			DatiP dati = null;
			
			try {
				fileInputStream = new FileInputStream("G:/Roba/Informatica/Info_2017/src/GestionaliProff/dati.dat");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			try {
				objectInputStream = new ObjectInputStream(fileInputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			try {

				dati =  (DatiP) objectInputStream.readObject();
				System.out.println(dati);

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			try {
				objectInputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return dati;
		}

		
		public static void main(String[] args) {
			 AlunniGUI a = new AlunniGUI();
			
		}
}


