package GestionaliProff;

import java.io.Serializable;
//Serializable serve per salvare su file un elemnto di tipo Alunno

public class Alunno implements Serializable{
	private Classe classe;
	private String nome;
	private String cognome;
	
	public Alunno(String nome,String cognome){
		this.nome=nome;
		this.cognome=cognome;
		
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	public String toString(){
		return nome + " "+ cognome +" "+classe; 
	}
	
	public static void main(String[] args) {
		Alunno alunno = new Alunno("Roberto","Garibaldi");
		Classe classe = new Classe(5,'A',"Informatica");
		alunno.setClasse(classe);
		
		System.out.println(alunno);
	}

}



