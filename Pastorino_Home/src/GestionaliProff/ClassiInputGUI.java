package GestionaliProff;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class ClassiInputGUI {
	
	private JFrame frame;
	private Vector<Classe> classi;

	public ClassiInputGUI() {
		super();
		
		classi = new Vector<>();
		
		frame = new JFrame("ClassiInputGUI");
		
		JPanel southPanel = new JPanel();
		JButton addButton = new JButton("ADD");
		JButton cancelButton = new JButton("CANCEL");
		JButton saveButton = new JButton("SAVE AND EXIT");
		
		southPanel.add(addButton);
		southPanel.add(cancelButton);
		southPanel.add(saveButton);
		
		frame.getContentPane().add(southPanel, BorderLayout.SOUTH);
		
		JPanel centerPanel = new JPanel();
		Box centerBox = Box.createVerticalBox();
		centerBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
		Box annoBox = Box.createHorizontalBox();
		Box sezioneBox = Box.createHorizontalBox();
		Box indirizzoBox = Box.createHorizontalBox();
		
		JLabel annoLabel = new JLabel("ANNO:");
		JTextField annoField = new JTextField(1);
		annoBox.add(annoLabel);
		annoBox.add(Box.createRigidArea(new Dimension(10, 0))); 
		annoBox.add(annoField);
		
		JLabel sezioneLabel = new JLabel("SEZIONE");
		JTextField sezioneField = new JTextField(1);
		sezioneBox.add(sezioneLabel);
		sezioneBox.add(Box.createRigidArea(new Dimension(10, 0))); 
		sezioneBox.add(sezioneField);
		
		JLabel indirizzoLabel = new JLabel("INDIRIZZO");
		JTextField indirizzoField = new JTextField(10);
		indirizzoBox.add(indirizzoLabel);
		indirizzoBox.add(Box.createRigidArea(new Dimension(10, 0))); 
		indirizzoBox.add(indirizzoField);
		
		centerBox.add(annoBox);
		centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
		centerBox.add(sezioneBox);
		centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
		centerBox.add(indirizzoBox);
		
		centerPanel.add(centerBox);
		
		frame.getContentPane().add(centerPanel);
		
		frame.setBounds(400, 200, 300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String anno = annoField.getText();
				if(anno.trim().equals("")) return;
				
				String sezione = sezioneField.getText();
				if(sezione.trim().equals("")) return;
				
				String indirizzo = indirizzoField.getText();
				if(indirizzo.trim().equals("")) return;
				
				classi.add(new Classe(Integer.parseInt(anno), sezione.charAt(0), indirizzo));
				
				annoField.setText("");
				sezioneField.setText("");
				indirizzoField.setText("");
				
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				annoField.setText("");
				sezioneField.setText("");
				indirizzoField.setText("");
				
			}
		});
		
		saveButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				try {
					FileOutputStream out;
					out = new FileOutputStream("G:/Roba/Informatica/Info_2017/src/GestionaliProff/classi.dat");
					ObjectOutputStream outputStream = new ObjectOutputStream(out);
					outputStream.writeObject(classi);
					outputStream.flush();
					outputStream.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				frame.dispose();
				
			}
		});
	}
	
	public static void main(String[] args) {
		new ClassiInputGUI();
		
	}

}
