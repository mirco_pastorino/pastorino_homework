package Interfacce_Grafiche;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class FlowLayoutDemo3 {

	public static void main(String[] args) {
		FlowLayout l = new FlowLayout();
		JFrame j = new JFrame("Test");
		JLabel la = new JLabel("Label");
		JButton b = new JButton("Button");
		j.setLayout(l);
		j.add(la);
		j.add(b);
		j.setVisible(true);
		j.setSize(200,200);
	}

}
