package Interfacce_Grafiche;

public interface Comparable {
	int compareTo(Comparable o);
}