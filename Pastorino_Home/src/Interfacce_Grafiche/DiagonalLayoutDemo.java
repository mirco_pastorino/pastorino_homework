package Interfacce_Grafiche;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class DiagonalLayoutDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame j = new JFrame("Test");
		//JLabel la = new JLabel("Label");
		//la.setOpaque(true);
		//la.setBackground(Color.CYAN);
		//la.setBounds(100, 100, 200, 50);
		JButton b  = new JButton("Button");
		JButton b1 = new JButton("Button");
		JButton b2 = new JButton("Button");
		JButton b3 = new JButton("Button");
		JButton b4 = new JButton("Button");
		b.setBounds(200, 500, 200, 50);
		b1.setBounds(200, 500, 200, 50);
		b2.setBounds(200, 500, 200, 50);
		b3.setBounds(200, 500, 200, 50);
		b4.setBounds(200, 500, 200, 50);
		j.setLayout(new DiagonalLayout());
		//j.add(la);
		j.add(b);
		j.add(b1);
		j.add(b2);
		j.add(b3);
		j.add(b4);
		j.setVisible(true);
		j.setSize(200,200);


	}

}
