package Interfacce_Grafiche;

import javax.swing.*;

public class CircleLayoutDemo {
	
	public CircleLayoutDemo(){
		
		JFrame f = new JFrame();
		JPanel p = new JPanel();
		JButton b = new JButton("1");
		JButton b1 = new JButton("2");
		JButton b2 = new JButton("3");
		JButton b3 = new JButton("4");
		
		f.add(p);
		p.setLayout(new CircleLayout());
		p.add(b);
		p.add(b1);
		p.add(b2);
		p.add(b3);
		b.setBounds(200, 500, 200, 50);
		b1.setBounds(200, 500, 200, 50);
		b2.setBounds(200, 500, 200, 50);
		b3.setBounds(200, 500, 200, 50);
		f.setVisible(true);
		f.setSize(200,200);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CircleLayoutDemo m = new CircleLayoutDemo();

	}

}
