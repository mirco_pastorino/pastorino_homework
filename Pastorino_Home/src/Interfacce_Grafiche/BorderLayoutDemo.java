package Interfacce_Grafiche;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class BorderLayoutDemo {

	public static void main(String[] args) {
		BorderLayout l = new BorderLayout();
		JFrame j = new JFrame("Test");
		JLabel la = new JLabel("Label");
		JButton b = new JButton("Button");
		JButton button = new JButton("button2");
		JPanel pan = new JPanel();
		
		j.setLayout(l);
		j.add(la);
		pan.add(b);
		pan.add(button);
		j.add(pan,BorderLayout.SOUTH);
		j.setVisible(true);
		j.setSize(200,200);
	}

}

