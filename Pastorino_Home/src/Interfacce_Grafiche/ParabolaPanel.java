package Interfacce_Grafiche;



import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class ParabolaPanel extends JPanel{
	int y1,y2,x1,x2;
	double a,b,c;
	boolean clear = false;
	public ParabolaPanel(double a,double b,double c){
		this.a = a;
		this.b = b;
		this.c = c;
		repaint();
	}
	public void setA(double a) {
		this.a = a;
	}

	public void setB(double b) {
		this.b = b;
	}

	public void setC(double c) {
		this.c = c;
	}

	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int ofst = 500;
		for(int i=-1000;i<1000;i++){
			y1 = (int) (-a*(i*i)+-b*i+-c);
			y2 = (int) (-a*((i+1)*(i+1))+-b*(i+1)+-c);
			x1=i;
			x2=i+1;
		g.drawLine(0, 500, 1024, 500);
		g.drawLine(500, 0, 500, 1024);
		
		g.drawLine(x1+ofst, y1+ofst, x2+ofst, y2+ofst);
		}
	}
}