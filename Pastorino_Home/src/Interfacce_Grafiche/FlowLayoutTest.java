package Interfacce_Grafiche;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FlowLayoutTest extends JFrame{

	public FlowLayoutTest() {
		super("Flow Layout");
		Container c = this.getContentPane();
	
		FlowLayout l = new FlowLayout();
		c.setLayout(l);
		
		
        for(int i =1;i<10;i++){
			
			c.add(new JButton(String.valueOf(i)));
			
			setSize(200,200);
			setVisible(true);
			
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
					
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		FlowLayoutTest a = new FlowLayoutTest();
	}

}
