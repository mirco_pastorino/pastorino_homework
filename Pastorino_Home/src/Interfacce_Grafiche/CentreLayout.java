package Interfacce_Grafiche;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class CentreLayout implements LayoutManager {

	private Component[] components;

	@Override
	public void addLayoutComponent(String name, Component comp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeLayoutComponent(Component comp) {
		// TODO Auto-generated method stub

	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void layoutContainer(Container parent) {
		// TODO Auto-generated method stub
		System.out.println("layoutContainer");
		components = parent.getComponents();
		
		for (int i = 0; i < components.length; i++) {
			int width =(parent.getWidth())/10;
			int x = (parent.getHeight())/2;
			int y = (parent.getWidth())/2;
			int height = (parent.getHeight())/10;
			components[i].setBounds(x, y, width, height);
		}

	}

}
