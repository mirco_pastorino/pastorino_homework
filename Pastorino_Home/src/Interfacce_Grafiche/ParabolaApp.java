package Interfacce_Grafiche;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;



public class ParabolaApp extends JFrame{
	ParabolaPanel parabolaPanel = new ParabolaPanel(0.001,0,0);
	JTextField t[] = new JTextField[3];
	public ParabolaApp(){
		super("ParabolaApp");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		JPanel textFieldPanel = new JPanel();
		textFieldPanel.setLayout(new GridLayout(3,1,0,200));
		for(int i=0;i<3;i++){
			t[i] = new JTextField("                ");
			if(i==0)t[i].addActionListener(new ListenerA());
			else if(i==1)t[i].addActionListener(new ListenerB());
			else t[i].addActionListener(new ListenerC());
			textFieldPanel.add(t[i]);
		}
		c.add(textFieldPanel,BorderLayout.EAST);
		c.add(parabolaPanel,BorderLayout.CENTER);
		
		setVisible(true);
		

		setSize(1280,1024);
	}
	class ListenerA implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			parabolaPanel.setA(Double.parseDouble(t[0].getText()));
			parabolaPanel.repaint();
		}
		
	}
	class ListenerB implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			parabolaPanel.setB(Double.parseDouble(t[1].getText()));
			parabolaPanel.repaint();
		}
		
	}
	class ListenerC implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			parabolaPanel.setC(Double.parseDouble(t[2].getText()));
			parabolaPanel.repaint();
		}
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new ParabolaApp();

			}
		});
	}

}
