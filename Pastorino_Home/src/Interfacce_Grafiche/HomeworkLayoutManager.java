package Interfacce_Grafiche;
import javax.swing.*;
import java.awt.*;

public class HomeworkLayoutManager{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JFrame finestra = new JFrame("Layout Manager");
		
		JPanel pan1 = new JPanel();
		JPanel pan2 = new JPanel();
		
		BorderLayout borderLayout = new BorderLayout();
		GridLayout gridLayout = new GridLayout(3,2);
		FlowLayout flowLayout = new FlowLayout();
		
		JButton b1 = new JButton("A");
		JButton b2= new JButton("B");
		JButton b3 = new JButton("C");
		JButton b4 = new JButton("D");
		JButton b5 = new JButton("E");
		JButton b6 = new JButton("F");
		JButton p1button1 = new JButton("PanelButton 1");
		JButton p1button2 = new JButton("PanelButton 2");
		JButton p1button3 = new JButton("PanelButton 3");
		JButton west = new JButton("West");
		JButton east = new JButton("East");
		JButton south = new JButton("Border button south");
		
		finestra.setVisible(true);
		finestra.setSize(450,200);
		finestra.setDefaultCloseOperation(finestra.EXIT_ON_CLOSE );
		
		finestra.setLayout(borderLayout);
		pan1.setLayout(flowLayout);
		pan1.setVisible(true);
		pan2.setLayout(gridLayout);
		pan2.setVisible(true);
		
		finestra.add(pan1, BorderLayout.NORTH);
		
		p1button1.setBounds(30,100,5,5);
		p1button2.setBounds(30,100,5,5);
		p1button3.setBounds(30,100,5,5);
		
		pan1.add(p1button1);	
		pan1.add(p1button2);	
		pan1.add(p1button3);		
		
		finestra.add(pan2, BorderLayout.CENTER);
		
		pan2.add(b1);
		pan2.add(b2);
		pan2.add(b3);
		pan2.add(b4);
		pan2.add(b5);
		pan2.add(b6);
		
		finestra.add(east, BorderLayout.LINE_START);
		finestra.add(west, BorderLayout.LINE_END);
		finestra.add(south, BorderLayout.SOUTH);
		
		
	}

}

