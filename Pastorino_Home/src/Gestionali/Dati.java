package Gestionali;

import java.io.Serializable;
import java.util.Vector;



public class Dati implements Serializable {

	private Vector<Alunno> alunni;
	private Vector<Classe> classi;
	
	//aggiungere alunni
	public boolean add(Alunno alunno) {
		return alunni.add(alunno);
	}

	//aggiungere classi
	public boolean add(Classe e) {
		return classi.add(e);
	}

	public Dati() {
		super();
		alunni = new Vector<>();
		classi = new Vector<>();
	}

	@Override
	public String toString() {
		return "Dati [alunni=" + alunni + ", classi=" + classi + "]";
	}

	public Classe[] getClassi() {
		// TODO Auto-generated method stub
		System.out.println(classi);
		Classe[] c = new Classe[0];
		return classi.toArray(c);
	}

	public Alunno[] getAlunni() {
		// TODO Auto-generated method stub
		Alunno[] c = new Alunno[0];
		return alunni.toArray(c);
	}

	
}

