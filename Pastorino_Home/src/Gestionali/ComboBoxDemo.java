package Gestionali;
//Demonstrate a simple combo box. 

import javax.swing.*;  
import java.awt.*; 
import java.awt.event.*; 

class ComboBoxDemo {  

	JComboBox jcbb; 
	JLabel jlab; 

	// Create an array of apple varieties. 
	String apples[] = { "Winesap", "Cortland", "Red Delicious",  
			"Golden Delicious", "Gala", "Fuji", 
			"Granny Smith", "Jonathan" }; 

	ComboBoxDemo() {  
		// Create a new JFrame container. 
		JFrame jfrm = new JFrame("JComboBox Demo");  

		// Specify FlowLayout manager.   
		jfrm.getContentPane().setLayout(new FlowLayout());  

		// Give the frame an initial size.  
		jfrm.setSize(220, 240);  

		// Terminate the program when the user closes the application.  
		jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  

		// Create a JComboBox 
		jcbb = new JComboBox(apples); 

		// Make a label that displays the selection. 
		jlab = new JLabel(); 

		// Add action listener for the combo box. 
		jcbb.addActionListener(new ActionListener() {  
			public void actionPerformed(ActionEvent le) {  
				// Get a reference to the item selected. 
				String item = (String) jcbb.getSelectedItem(); 

				// Display the selected item. 
				jlab.setText("Current selection " + item); 
			}  
		});  

		// Initially select the first item in the list. 
		jcbb.setSelectedIndex(0); 

		// Add the combo box and label to the content pane. 
		jfrm.getContentPane().add(jcbb); 
		jfrm.getContentPane().add(jlab); 

		// Display the frame.  
		jfrm.setVisible(true);  
	}  

	public static void main(String args[]) {  
		// Create the frame on the event dispatching thread.  
		SwingUtilities.invokeLater(new Runnable() {  
			public void run() {  
				new ComboBoxDemo();  
			}  
		});   
	}  
}

