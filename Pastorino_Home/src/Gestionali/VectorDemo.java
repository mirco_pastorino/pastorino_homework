package Gestionali;

import java.util.Vector;

public class VectorDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Vettore normale
		int[] numeri = new int[3];
		numeri[0]=33;
		numeri[1]=2;
		numeri[2]=43;
		
		//Vector 
		Vector<Integer> numeri2 =new Vector<>();
		numeri2.add(33);
		numeri2.add(2);
		numeri2.add(43); //il vector si ridimensione automaticamnete
		
		
		System.out.println(numeri2);
		
		numeri2.remove(2);              //2 sta per la posizione
		System.out.println(numeri2);
		
		Integer n = new Integer(33);
		System.out.println(numeri2.indexOf(n));
		
		for (Integer integer : numeri2){      //ciclo Vector semplice
			System.out.println(integer);
		}
		
		/*for(int i=0; i<numeri2.get(i);i++){   //ciclo Vector inutile
			Integer integer = numeri2.get(i);
			System.out.println(integer);
		}*/

	}

}
