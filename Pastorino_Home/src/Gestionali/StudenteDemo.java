package Gestionali;

import java.util.Vector;

public class StudenteDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Studente s1 = new Studente();
		Studente s2 = new Studente();
		Studente s3 = new Studente();
		Studente s4 = new Studente();
		
		s1.setCognome("Rossi");
		s1.setNome("Marco");
	    s2.setCognome("Verdi");
	    s2.setNome("Mario");
	    s3.setCognome("Violi");
	    s3.setNome("Massimo");
	    s4.setCognome("Azzurri");
	    s4.setNome("Mattia");
		
		
		Vector<Studente> studenti =new Vector<>();
		studenti.add(s1);
		studenti.add(s2);
		studenti.add(s3);
		studenti.add(s4);
		
		System.out.println(studenti.indexOf(s1));
		
		

	}

}
