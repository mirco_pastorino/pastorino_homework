package Gestionali;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Gestionali.GUI_Scuola.ListenerA;
import Gestionali.GUI_Scuola.ListenerB;
import Gestionali.GUI_Scuola.ListenerC;
import GestionaliProff.DatiP;

public class AlunniInputGUI extends JFrame{

	JTextField t[]= new JTextField[2];
	public static Dati dati;
	
	public AlunniInputGUI(){
		
	super("Scuola");
    //dati = readDati();
	Container c = getContentPane();
	c.setLayout(new BorderLayout());
	JLabel l = new JLabel("Nome");
	JLabel l1 = new JLabel("Cognome");

	
	JPanel p = new JPanel();
	JPanel p1 = new JPanel();
	
	JButton b = new JButton("Salva");
	b.addActionListener(new ListenerA());
	JButton b1 = new JButton("Cancella");
	b1.addActionListener(new ListenerB());
	JButton b2 = new JButton("Salva ed esci");
	b2.addActionListener(new ListenerC());
	
	t[0]=new JTextField(10);
	t[1]=new JTextField(10);

	Box box1 = Box.createVerticalBox(); 
	Box box2 = Box.createVerticalBox(); 
	Box box3 = Box.createHorizontalBox(); 

	
	box1.setBorder( 
			BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
	box2.setBorder( 
			BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
	box3.setBorder( 
			BorderFactory.createEmptyBorder(10, 10, 10, 10)); 

	
	box1.add(l); 
	box1.add(Box.createRigidArea(new Dimension(0, 4))); 
	box1.add(l1); 
	box1.add(Box.createRigidArea(new Dimension(0, 4))); 

			
    p.add(box1);
    p.add(box2);
	
    box2.add(t[0]);
    box2.add(t[1]);
	
	
	box3.add(b);
	box3.add(b1);
	box3.add(b2);
	
	p1.add(box3);
	c.add(p1,BorderLayout.SOUTH);
	c.add(p,BorderLayout.NORTH);
    setVisible(true);
    setSize(800,600);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
}
	
	private Dati readDati() {
		
		ObjectInputStream objectInputStream = null;
		FileInputStream fileInputStream = null;

		
		try {
			fileInputStream = new FileInputStream("G:/Roba/Informatica/Info_2017/src/Gestionali/dati.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		try {
			objectInputStream = new ObjectInputStream(fileInputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {

			dati =  (Dati) objectInputStream.readObject();
			System.out.println(dati);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			objectInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dati;
	}


	class ListenerA implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			 String nome = t[0].getText();
		     
		     String cognome = t[1].getText();
		        
		     dati.add(new Alunno(nome,cognome));
		        
		      t[0].setText("");
		      t[1].setText("");
		}
		
	}
	
	class ListenerB implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			for(int i=0;i<t.length;i++) System.out.println("");
		}
		
	}
	
	static class ListenerC implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			try
	        {
	          FileOutputStream out = new FileOutputStream("G:/Roba/Informatica/Info_2017/src/Gestionali/dati.dat");
	          ObjectOutputStream outputStream = new ObjectOutputStream(out);
	          outputStream.writeObject(dati);
	          outputStream.flush();
	          outputStream.close();
	        }
	        catch (IOException e1) {
	          e1.printStackTrace();
	        } System.exit(0);
		}
		
     

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AlunniInputGUI f = new AlunniInputGUI();

	}

   }

}