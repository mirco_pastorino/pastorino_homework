package Gestionali;



import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.io.*;

public class FormazioneClassiGUI implements Serializable{
	
		
		private JFrame frame;
		//private Vector<Classe> classi;
		private Dati dati;
		
		private Classe[] classi;
		
		private Alunno[] alunni;
		
		public FormazioneClassiGUI() {
			super();
			
			//classi = new Vector<>();
			dati = readDati();
			
			alunni = dati.getAlunni();
						
			classi = dati.getClassi();
			
			frame = new JFrame("ClassiInputGUI2");
			
			JPanel southPanel = new JPanel();
			JButton setButton = new JButton("SET");
			JButton saveButton = new JButton("SAVE AND EXIT");
			
			southPanel.add(setButton);
			southPanel.add(saveButton);
			
			frame.getContentPane().add(southPanel, BorderLayout.SOUTH);
			
			JPanel centerPanel = new JPanel();
			Box centerBox = Box.createVerticalBox();
			centerBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
			Box alunnoBox= Box.createHorizontalBox();
			Box classeBox= Box.createHorizontalBox();
			
			JLabel alunnoLabel = new JLabel("ALUNNO");
			JComboBox alunnoComboBox = new JComboBox(alunni);
			alunnoComboBox.setSelectedIndex(-1);
			classeBox.add(alunnoLabel);
			classeBox.add(Box.createRigidArea(new Dimension(10, 0))); 
			classeBox.add(alunnoComboBox);
			
			JLabel classeLabel = new JLabel("CLASSE");
			JComboBox classeComboBox = new JComboBox(classi);
			classeComboBox.setSelectedIndex(-1);
			classeBox.add(classeLabel);
			classeBox.add(Box.createRigidArea(new Dimension(10, 0))); 
			classeBox.add(classeComboBox);
			
			centerBox.add(alunnoBox);
			centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
			centerBox.add(classeBox);
			centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
			
			
			centerPanel.add(centerBox);
			
			frame.getContentPane().add(centerPanel);
			
			frame.setBounds(400, 200, 300, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
			
			setButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					Alunno alunno = (Alunno) alunnoComboBox.getSelectedItem();
					if(alunno == null) return;
					
					Classe classe = (Classe) classeComboBox.getSelectedItem();
					if(classe == null) return;
				
					alunno.setClasse(classe);
					
					alunnoComboBox.setSelectedIndex(-1);
					classeComboBox.setSelectedIndex(-1);
					
				}
			});
			
			saveButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					System.out.println(dati);
					
					try {
						FileOutputStream out;
						out = new FileOutputStream("G:/Roba/Informatica/Info_2017/src/Gestionali/dati.dat");
						ObjectOutputStream outputStream = new ObjectOutputStream(out);
						//outputStream.writeObject(classi);
						outputStream.writeObject(dati);
						outputStream.flush();
						outputStream.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					System.exit(1);
					
					//frame.dispose();
					
				}
			});
		}
		/*
		 *lettura di dati da file
		 */
		public Dati readDati() {
			
			ObjectInputStream objectInputStream = null;
			FileInputStream fileInputStream = null;
			Dati dati = null;
			
			try {
				fileInputStream = new FileInputStream("G:/Roba/Informatica/Info_2017/src/Gestionali/dati.dat");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				objectInputStream = new ObjectInputStream(fileInputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			try {

				dati =   (Dati) objectInputStream.readObject();
				System.out.println(dati);

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			try {
				objectInputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return dati;
		}

		
		public static void main(String[] args) {
			FormazioneClassiGUI fr=new FormazioneClassiGUI();
			
		}

}
