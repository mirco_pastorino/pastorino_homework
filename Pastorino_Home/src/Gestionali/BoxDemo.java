package Gestionali;
//Demonstrate BoxLayout and the Box class. 

import java.awt.*;   
import javax.swing.*;   

class BoxDemo { 

	BoxDemo() { 

		// Create a new JFrame container. 
		JFrame jfrm = new JFrame("BoxLayout Demo"); 

		// *** Use FlowLayout for the content pane. *** 
		jfrm.getContentPane().setLayout(new FlowLayout());     

		// Give the frame an initial size.   
		jfrm.setSize(300, 240); 

		// Terminate the program when the user closes the application. 
		jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

		// Make the labels. 
		JLabel jlabOne = new JLabel("Button Group One"); 
		JLabel jlabTwo = new JLabel("Button Group Two"); 
		JLabel jlabThree = new JLabel("Check Box Group"); 

		// Make the buttons. 
		JButton jbtnOne = new JButton("One"); 
		JButton jbtnTwo = new JButton("Two"); 
		JButton jbtnThree = new JButton("Three"); 
		JButton jbtnFour = new JButton("Four"); 
		Dimension btnDim = new Dimension(50, 25); 

		// Set minimum and maximum sizes for the buttons. 
		Dimension dim = new Dimension(200, 10);
//		jbtnOne.setMinimumSize(dim);  
		jbtnOne.setMaximumSize(btnDim); 
		jbtnOne.setPreferredSize(dim);
		System.out.println(jbtnOne.getPreferredSize());
		System.out.println(jbtnOne.getMinimumSize());
		System.out.println(jbtnOne.getMaximumSize());
		jbtnTwo.setMinimumSize(btnDim);  
		jbtnTwo.setMaximumSize(btnDim); 
		jbtnThree.setMinimumSize(btnDim);  
		jbtnThree.setMaximumSize(btnDim); 
		jbtnFour.setMinimumSize(btnDim);  
		jbtnFour.setMaximumSize(btnDim); 
		

		// Make the checkboxes. 
		JCheckBox jcbOne = new JCheckBox("Option One"); 
		JCheckBox jcbTwo = new JCheckBox("Option Two"); 

		// Create three vertical boxes. 
		Box box1 = Box.createVerticalBox(); 
		Box box2 = Box.createVerticalBox(); 
		Box box3 = Box.createVerticalBox(); 

		// Create invisible borders around the boxes. 
		box1.setBorder( 
				BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
		box2.setBorder( 
				BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
		box3.setBorder( 
				BorderFactory.createEmptyBorder(10, 10, 10, 10)); 

		// Add the components to the boxes. 
		box1.add(jlabOne); 
		box1.add(Box.createRigidArea(new Dimension(0, 4))); 
		box1.add(jbtnOne); 
		box1.add(Box.createRigidArea(new Dimension(0, 4))); 
		box1.add(jbtnTwo); 

		box2.add(jlabTwo); 
		box2.add(Box.createRigidArea(new Dimension(0, 4))); 
		box2.add(jbtnThree); 
		box2.add(Box.createRigidArea(new Dimension(0, 4))); 
		box2.add(jbtnFour); 

		box3.add(jlabThree); 
		box3.add(jcbOne); 
		box3.add(jcbTwo); 

		// Add the boxes to the content pane. 
		jfrm.getContentPane().add(box1); 
		jfrm.getContentPane().add(box2); 
		jfrm.getContentPane().add(box3); 

		// Display the frame.   
		jfrm.setVisible(true);   
	}   

	public static void main(String args[]) {   
		// Create the frame on the event dispatching thread.   
		SwingUtilities.invokeLater(new Runnable() {   
			public void run() {   
				new BoxDemo();   
			}   
		});   
	}   
}
