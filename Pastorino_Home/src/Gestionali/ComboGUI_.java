package Gestionali;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Gestionali.GUI_Scuola.ListenerA;
import Gestionali.GUI_Scuola.ListenerB;
import Gestionali.GUI_Scuola.ListenerC;

public class ComboGUI_ extends JFrame{

	private Vector<Classe> classi = new Vector<>();

	String anni[]= {"1", "2", "3", "4", "5"};
	String sezioni[]= {"A", "B", "C", "D"};
	String indirizzi[]= {"Informatica", "Meccanica", "Energie", "Elettronica", "Meccanica"};
		
	JComboBox janno = new JComboBox(anni);
	JComboBox jsez = new JComboBox(sezioni);
	JComboBox jind = new JComboBox(indirizzi);
	
	
	public ComboGUI_(){
		super("Scuola");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		JLabel l = new JLabel("Anno");
		JLabel l1 = new JLabel("Sezione");
		JLabel l2 = new JLabel("Indirizzo");
		
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		
		JButton b = new JButton("Salva");
		b.addActionListener(new ListenerA());
		JButton b1 = new JButton("Cancella");
		b1.addActionListener(new ListenerB());
		JButton b2 = new JButton("Salva ed esci");
		b2.addActionListener(new ListenerC());
	
		Box box1 = Box.createVerticalBox(); 
		Box box2 = Box.createVerticalBox(); 
		Box box3 = Box.createVerticalBox(); 

		
		box1.setBorder( 
				BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
		box2.setBorder( 
				BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
		box3.setBorder( 
				BorderFactory.createEmptyBorder(10, 10, 10, 10)); 

		
		box1.add(l); 
		box1.add(Box.createRigidArea(new Dimension(0, 4))); 
		box1.add(l1); 
		box1.add(Box.createRigidArea(new Dimension(0, 4))); 
		box1.add(l2); 
				
	    p.add(box1);
	    p.add(box2);
		
		box2.add(janno);
		box2.add(jsez);
		box2.add(jind);
		

	    p1.add(b);
		p1.add(b1);
		p1.add(b2);
		
		c.add(p1,BorderLayout.SOUTH);
		c.add(p,BorderLayout.NORTH);
        setVisible(true);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	class ListenerA implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			 String anno = (String) janno.getSelectedItem();
		     
		     String sezione = (String) jsez.getSelectedItem();
		        
		     String indirizzo = (String) jind.getSelectedItem();
		        
		     classi.add(new Classe(Integer.parseInt(anno), sezione, indirizzo));
		        
		      
		}
		
	}
	
	class ListenerB implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			 System.out.println("");
		}
		
	}
	
	class ListenerC implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			try
	        {
	          FileOutputStream out = new FileOutputStream("H:/Roba/Informatica/Info_2017/src/Gestionali/classi.dat");
	          ObjectOutputStream outputStream = new ObjectOutputStream(out);
	          outputStream.writeObject(classi);
	          outputStream.flush();
	          outputStream.close();
	        }
	        catch (IOException e1) {
	          e1.printStackTrace();
	        }
			System.exit(0);
		} 
		
		
	}
	public static void main(String[] args) {
        
		ComboGUI_ g = new ComboGUI_();
	}


}
