package Gestionali;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.*;

public class GUI_Scuola extends JFrame{
	
	private Vector<Classe> classi = new Vector<>();

	JTextField t[] = new JTextField[3];
	
	public GUI_Scuola(){
		super("Scuola");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		JLabel l = new JLabel("Anno");
		JLabel l1 = new JLabel("Sezione");
		JLabel l2 = new JLabel("Indirizzo");
		
		
		JButton b = new JButton("Salva");
		b.addActionListener(new ListenerA());
		JButton b1 = new JButton("Cancella");
		b1.addActionListener(new ListenerB());
		JButton b2 = new JButton("Salva ed esci");
		b2.addActionListener(new ListenerC());
		GridLayout g = new GridLayout(3,2);
		
		JPanel p1 = new JPanel();
		p1.setLayout(g);
		JPanel annoP = new JPanel();
		p1.add(annoP);
		JPanel sezP = new JPanel();
		p1.add(sezP);
		JPanel indP = new JPanel();
		p1.add(indP);
		JPanel p4 = new JPanel();
				
		t[0]=new JTextField(10);
		t[1]=new JTextField(10);
		t[2]=new JTextField(10);
		
		annoP.add(l);
		annoP.add(t[0]);
		sezP.add(l1);
		sezP.add(t[1]);
		indP.add(l2);
		indP.add(t[2]);
		
//		for(int i=0;i<t.length;i++){
//			p1.add(t[i]);	
//		}	
		p4.add(b);
		p4.add(b1);
		p4.add(b2);
		
		c.add(p4,BorderLayout.SOUTH);
		c.add(p1,BorderLayout.NORTH);
        setVisible(true);
        setSize(1280,1024);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	class ListenerA implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			 String anno = t[0].getText();
		     
		     String sezione = t[1].getText();
		        
		     String indirizzo = t[2].getText();
		        
		     classi.add(new Classe(Integer.parseInt(anno), sezione, indirizzo));
		        
		      t[0].setText("");
		      t[1].setText("");
		      t[2].setText("");
		}
		
	}
	
	class ListenerB implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			for(int i=0;i<t.length;i++) System.out.println("");
		}
		
	}
	
	class ListenerC implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			try
	        {
	          FileOutputStream out = new FileOutputStream("src/Stream/classi.dat");
	          ObjectOutputStream outputStream = new ObjectOutputStream(out);
	          outputStream.writeObject(classi);
	          outputStream.flush();
	          outputStream.close();
	        }
	        catch (IOException e1) {
	          e1.printStackTrace();
	        }
		} 
		
		
	}
	public static void main(String[] args) {
        
		GUI_Scuola g = new GUI_Scuola();
	}


}
