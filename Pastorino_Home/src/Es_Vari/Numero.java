package Es_Vari;

import Interfacce_Grafiche.Comparable;

public class Numero implements Comparable {
	public int numero;
	public Numero(int numero)
	{
		this.numero = numero;
	}
	@Override
	public int compareTo(Comparable o) {
		return (this.numero)-((Numero)o).numero;
	}
}