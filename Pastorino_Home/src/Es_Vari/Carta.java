package Es_Vari;

import Interfacce_Grafiche.Comparable;

public class Carta implements Comparable {

	private String seme;
	private int numero;
	final int bastoni=0, spade=1, coppe=2, ori=3;

	public Carta(String seme, int numero) {
		this.seme = seme;
		this.numero = numero;
	}

	@Override
	public String toString() {
		return numero+" di "+seme;
	}

	@Override
	public int compareTo(Comparable o) {

		return 0;
	}


	}

