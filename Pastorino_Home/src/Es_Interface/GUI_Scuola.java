package Es_Interface;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import Interfacce_Grafiche.ParabolaApp;

public class GUI_Scuola extends JFrame{
	JTextField t[] = new JTextField[3];
	public GUI_Scuola(){
		super("Scuola");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		JLabel l = new JLabel("Anno");
		JLabel l1 = new JLabel("Sezione");
		JLabel l2 = new JLabel("Indirizzo");
		
		JButton b = new JButton("Salva");
		b.addActionListener(new ListenerA());
		JButton b1 = new JButton("Cancella");
		b1.addActionListener(new ListenerB());
		GridLayout g = new GridLayout(3,2);
		
		JPanel p1 = new JPanel();
		p1.setLayout(g);
		JPanel annoP = new JPanel();
		p1.add(annoP);
		JPanel sezP = new JPanel();
		p1.add(sezP);
		JPanel indP = new JPanel();
		p1.add(indP);
		JPanel p4 = new JPanel();
				
		t[0]=new JTextField(10);
		t[1]=new JTextField(10);
		t[2]=new JTextField(10);
		
		annoP.add(l);
		annoP.add(t[0]);
		sezP.add(l1);
		sezP.add(t[1]);
		indP.add(l2);
		indP.add(t[2]);
		
//		for(int i=0;i<t.length;i++){
//			p1.add(t[i]);	
//		}	
		p4.add(b);
		p4.add(b1);
		
		c.add(p4,BorderLayout.SOUTH);
		c.add(p1,BorderLayout.NORTH);
        setVisible(true);
        setSize(1280,1024);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	class ListenerA implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			for(int i=0;i<t.length;i++) System.out.println(t[i].getText());
		}
		
	}
	
	class ListenerB implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			for(int i=0;i<t.length;i++) System.out.println("");
		}
		
	}
	public static void main(String[] args) {
        
		GUI_Scuola g = new GUI_Scuola();
	}


}
