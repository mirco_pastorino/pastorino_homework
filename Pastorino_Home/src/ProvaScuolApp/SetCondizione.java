package ProvaScuolApp;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.*;

public class SetCondizione extends JFrame{
	
    private Dati dati;
	private Classe[] classi;
	private Alunno[] alunni;
	private JPanel cards;
	private String[] esito= {"Amesso ","Non ammesso ","Sospeso "};

	
	public SetCondizione(){
		super("Esiti");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());;
		JPanel p = new JPanel();
		
		dati = readDati();
		alunni = dati.getAlunni();	
		classi = dati.getClassi();
		
		JLabel classeLabel = new JLabel("CLASSE");
		JComboBox classeCombo = new JComboBox(classi);
		JPanel classeBox=new JPanel();
        classeCombo.setEditable(false);
        
        classeCombo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				 CardLayout cl = (CardLayout)(cards.getLayout());
			     cl.show(cards, (String)classeCombo.getSelectedItem());
				
			}
		});
        
        cards = new JPanel(new CardLayout());
        JPanel card = new JPanel();
        JLabel al = new JLabel();
        JComboBox es = new JComboBox(esito);
        
        card.add(al);
        card.add(es);
        cards.add(card);
        
		classeBox.add(classeCombo);
		classeBox.add(classeLabel);
		
		p.add(classeBox);
		c.add(p,BorderLayout.NORTH);
		setBounds(400, 200, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
public Dati readDati() {
		
		ObjectInputStream objectInputStream = null;
		FileInputStream fileInputStream = null;
		Dati dati = null;
		
		try {
			fileInputStream = new FileInputStream("G:/Roba/Informatica/Info_2017/src/ProvaScuolApp/dati.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectInputStream = new ObjectInputStream(fileInputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {

			dati =   (Dati) objectInputStream.readObject();
			System.out.println(dati);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			objectInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dati;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SetCondizione();

	}

}
