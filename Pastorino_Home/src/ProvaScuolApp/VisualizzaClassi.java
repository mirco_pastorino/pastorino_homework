package ProvaScuolApp;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class VisualizzaClassi extends JFrame{
	
	JPanel cards; //a panel that uses CardLayout
    final static String BUTTONPANEL = "Classi";
    Dati dati = new Dati();
    
	
    public VisualizzaClassi(){
    	
    	Container c = new Container();
    	JPanel p = new JPanel(); 
        String comboBoxItems[] = { BUTTONPANEL };
        JComboBox cb = new JComboBox(comboBoxItems);
        JTextArea ta = new JTextArea();
        
        cb.setEditable(false);
        cb.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				 CardLayout cl = (CardLayout)(cards.getLayout());
			     cl.show(cards, (String)cb.getSelectedItem());
				
			}
		});
        p.add(cb);
        c.add(p,BorderLayout.NORTH);
        
        setBounds(400, 200, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
    	
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		VisualizzaClassi v = new VisualizzaClassi();

	}

}
