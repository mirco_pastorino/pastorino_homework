package ProvaScuolApp;

import java.io.Serializable;
import java.util.Vector;
//Serializable serve per salvare su file un elemnto di tipo Alunno

public class Alunno implements Serializable{
	private Classe classe;
	private String nome;
	private String cognome;
	private Vector<Condizione> condizione;
	
	public Alunno(String nome,String cognome){
		this.nome=nome;
		this.cognome=cognome;
		
	}
	
	void setCondizione(Vector<Condizione> condizione){
		this.condizione=condizione;
	}

	void setClasse(Classe classe) {
		this.classe = classe;
	}
	public String toString(){
		return nome + " "+ cognome ; 
	}
	
	 Classe getClasse() {
		return classe;
	}

	String getNome() {
		return nome;
	}

    String getCognome() {
		return cognome;
	}

	public static void main(String[] args) {
		Alunno alunno = new Alunno("Roberto","Garibaldi");
		Classe classe = new Classe(5,"A","Informatica");
		alunno.setClasse(classe);
		
		System.out.println(alunno);
	}

}



