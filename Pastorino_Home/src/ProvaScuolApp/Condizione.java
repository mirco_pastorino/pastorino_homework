package ProvaScuolApp;

public class Condizione {
	
	public String esito = "";
	public String annata = "";
	public Classe classe;
	public Alunno alunno;
	
	public Condizione(String esito,String annata,Alunno alunno){
		this.esito=esito;
		this.annata=annata;
		this.classe=alunno.getClasse();
	}

}
