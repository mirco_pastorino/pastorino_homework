package ProvaScuolApp;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class VisualizzaAlunni extends JFrame{
	private Dati dati;
	
    private Classe[] classi;
	
	private Alunno[] alunni;
	
	
	public VisualizzaAlunni(){
		super("Visualizzazione");
		
		Container c = new Container();
		
		dati = readDati(); 
		alunni = dati.getAlunni();	
		classi = dati.getClassi();
		
		
		
		JPanel southPanel = new JPanel();
		JButton exit = new JButton("EXIT");
		JTextArea j = new JTextArea(50,50);
		
		JPanel centerPanel = new JPanel();
		Box centerBox = Box.createVerticalBox();
		centerBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
		Box area= Box.createHorizontalBox();
		Box classeBox= Box.createHorizontalBox();
		
		JLabel classeLabel = new JLabel("CLASSE");
		JComboBox classeComboBox = new JComboBox(classi);
		classeComboBox.setSelectedIndex(-1);
		classeBox.add(classeLabel);
		classeBox.add(Box.createRigidArea(new Dimension(10, 0))); 
		centerBox.add(classeComboBox);
		
		centerBox.add(j);
		centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
		centerBox.add(j);
		centerBox.add(Box.createRigidArea(new Dimension(0, 10))); 
		
		southPanel.add(exit);
		getContentPane().add(southPanel, BorderLayout.SOUTH);
		centerPanel.add(centerBox);
		
		getContentPane().add(centerPanel);
		
		setBounds(400, 200, 300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		
		
		exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				System.exit(1);
				
			}
		});
	}
	/*
	 *lettura di dati da file
	 */
	public Dati readDati() {
		
		ObjectInputStream objectInputStream = null;
		FileInputStream fileInputStream = null;
		Dati dati = null;
		
		try {
			fileInputStream = new FileInputStream("G:/Roba/Informatica/Info_2017/src/ProvaScuolApp/dati.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectInputStream = new ObjectInputStream(fileInputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {

			dati =   (Dati) objectInputStream.readObject();
			System.out.println(dati);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			objectInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dati;
	}
		
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VisualizzaAlunni a = new VisualizzaAlunni();

	}

}
