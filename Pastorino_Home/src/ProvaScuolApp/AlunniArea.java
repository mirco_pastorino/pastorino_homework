package ProvaScuolApp;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class AlunniArea {
	
	private JFrame frame;
	private Dati dati;
	private Classe[] classi;
	private Alunno[] alunni;

	public AlunniArea() {
		super();
		
		dati = readDati();
		
		classi = dati.getClassi();
		alunni = dati.getAlunni();
		
		frame = new JFrame("AlunniArea");
		JComboBox classiBox = new JComboBox(classi);
		JLabel classiLabel = new JLabel("Classi: ");
		JPanel comboClassiPanel = new JPanel();
		JPanel areaPanel = new JPanel();
		JTextArea alunniText = new JTextArea();
		
		comboClassiPanel.add(classiLabel);
		comboClassiPanel.add(classiBox);
		
		classiBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {

				alunniText.setText("");
				
				for(int i=0; i<alunni.length; i++)
					if(alunni[i].getClasse().equals((Classe) classiBox.getSelectedItem()))
						alunniText.append(alunni[i].getNome()+alunni[i].getCognome()+"\n");				
			}
		});
		
		areaPanel.add(alunniText);
		
		frame.getContentPane().add(comboClassiPanel, BorderLayout.NORTH);
		frame.getContentPane().add(areaPanel, BorderLayout.CENTER);
		
		frame.setSize(500, 500);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
	}
	
	public Dati readDati() {
		
		ObjectInputStream objectInputStream = null;
		FileInputStream fileInputStream = null;
		Dati dati = null;
		
		try {
			fileInputStream = new FileInputStream("G:/Roba/Informatica/Info_2017/src/ProvaScuolApp/dati.dat");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			objectInputStream = new ObjectInputStream(fileInputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {

			dati =  (Dati) objectInputStream.readObject();
			System.out.println(dati);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		try {
			objectInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dati;
	}

	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				new AlunniArea();
				
			}
		});

	}

}
