package ProvaScuolApp;

import java.io.Serializable;

public class Classe implements Serializable{
	
	private int anno;
	private String sezione;
	private String indirizzo;
	
	public Classe(int anno, String sezione, String indirizzo) {
		super();
		this.anno = anno;
		this.sezione = sezione;
		this.indirizzo = indirizzo;
	}
	public int getAnno() {
		return anno;
	}
	public String getSezione() {
		return sezione;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	
	@Override
	public String toString() {
		return anno   + sezione   + indirizzo ;
	}
	
	
	

}
